'use strict'
const koaRouter = require('koa-router')
const router = new koaRouter()
const functions = require('./functions')
module.exports.set = (app) => {
    router.get('/', ctx => ctx.body = {msg: 'testing the ember to api'})
    router.get('/search', functions.searchEngine)
    app.use(router.routes()).use(router.allowedMethods())
}