'use strict'
const search = require('../engines/search')
const engines = require('../engines/engineImporter')
async function searchEngine(ctx) {
    try{
    const searchEngine = await selectEngine(ctx.request.query.e)
    const requestedData = await search.request(ctx.request.query.q, searchEngine, ctx.request.query.hasDate, ctx.request.query.t1,ctx.request.query.t2, ctx.request.query.p)
    const jsonData = await extractLinks(requestedData.data, searchEngine, ctx.request.query.q)
    ctx.body = jsonData
    }catch(e){
        console.log(e)
    }
}

// async function nextPageGet(ctx){
//     const searchEngine = ctx.request.query.e;
//     const requestedData = await searchNextPage
//     ctx.body = "hi"
// }

async function extractLinks(html, engine, query){
    return engine.extractLinks(html, query)
}

async function selectEngine(engineName){
    switch (engineName) {
        case 'google':
            return engines.google
        // case 'duckduckgo':
        //     return duckduckgo
        // case 'bing':
        //     return bing
    
        default:
            return engines.google
    }
}

module.exports = {
    searchEngine: searchEngine,
}