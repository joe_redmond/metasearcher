'use strict'
const cheerio = require('cheerio')
const axios = require('axios')
const fs = require('fs')
const defaultHostname = 'www.google.com'
const titleElement = 'div.vvjwJb'
const topElement = '.kCrYT a'
const nextPageElement = 'footer div div div a'
const searchPath = '/search?'
const pageRegex = 'start=?([0-9])*'
axios.defaults.timeout = 5000

const googleInfo = {
    hostname: defaultHostname,
    searchPath: searchPath,
    query: 'q',
    itemStart: 'start',
    time: 'tbs=cdr:',
    timeMin: 'cd_min:',
    timeMax: 'cd_max:'
}

const searchGoogle = (query, hasDate, timeStart='', timeEnd='', page=0) => {
    const cdrValue = (hasDate ? 1:0)
    const pageUrl = `${googleInfo.itemStart}=${page}`
    const url = `https://${googleInfo.hostname}${googleInfo.searchPath}${googleInfo.query}=${query}&${pageUrl}`
    return axios.get(url)
}

const extractLinks = (html, query) => {
    fs.writeFileSync('testfile.html', html)
    const data = [{links: [], images: [], firstPage: 'start=1', previousPage: '', nextPage: '', query: query}]
    const $ = cheerio.load(html);
    let elements = [];
    $(topElement).each((i, element) => {
        elements.push(element)
    })

    elements.forEach(element => {
        if (typeof element == "object") {
            const regexTest1 = "\/search?"
            const regexTest2 = "google.com"
            if (!$(element).attr('href').match(regexTest1) && !$(element).attr('href').match(regexTest2) && !$(element).find("div:nth-child(1)").text() == '') {
                data[0].links.push({
                    title: $(element).find(titleElement).text(),
                    link: $(element).attr('href').match('http.*\/')[0]
                })
            }
        }
    })
    let footer = [];
    $(nextPageElement).each((i, element) => {
        footer.push(element)
    })
    data[0].previousPage = $(footer[Math.ceil((footer.length -1) / 2)]).attr('href').match(pageRegex)[0]
    data[0].nextPage = $(footer[footer.length - 1]).attr('href').match(pageRegex)[0]
    if(data[0].previousPage == data[0].nextPage){data[0].previousPage = data[0].firstPage}
    return data
}


module.exports = {
    extractLinks: extractLinks,
    request: searchGoogle,
}

