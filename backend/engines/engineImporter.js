/**
 * This file allows you to import new engines that are created and export them all together
 */
'use strict'
const google = require('./google')
const bing = require('./bing')
const duckduckgo = require('./duckduckgo')

module.exports = {
    google: google,
    bing: bing,
    duckduckgo: duckduckgo,

}