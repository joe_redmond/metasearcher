'use strict'
const axios = require('axios');
const tor_axios = require('tor-axios');
const request = (query, engine, hasDate, timeStart='', timeEnd='', page=1) => {
   return engine.request(query, hasDate, timeStart, timeEnd, page)
}


module.exports = {
    request: request,
}