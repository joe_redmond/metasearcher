'use strict'
const koa = require('koa')
const app = new koa()
const controllers = require('./controllers/routes');
controllers.set(app)
app.listen(3000, () => {
    console.log('Server Started...')
})


